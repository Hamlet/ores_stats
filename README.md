### Ores Stats
![Ores Stats' screenshot](screenshot.png)  
**_Provides informations about ores' percentages collected on map generation._**

**Version:** 1.1.0  
**Source code's license:** [EUPL v1.2][1] or later.

**Dependencies:** default, sfinv (found in [Minetest Game][2])
**Supported:** [More Ores][3]

**NOTE:** To update the page's values switch to another tab.

__Advanced option:__
Settings -> All Settings -> Mods -> ores_stats


### Installation

Unzip the archive, rename the folder to ores_stats and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://github.com/minetest/minetest_game
[3]: https://forum.minetest.net/viewtopic.php?f=11&t=549
