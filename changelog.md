# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further features planned.



## [1.1.0] - 2020-08-19
### Added

	- Support for More Ores



## [1.0.0] - 2020-07-23
### Added

	- Inventory's page (switch tab to update).
	- Ores' relative values.
	- Option to change gold's value via Settings -> All settings -> Mods -> ores_stats

### Changed

	- Code rewritten from scratch.
	- Version updated to 1.x, non backward compatible.

### Removed

	- Console and chat output.



## [0.2.0] - 2019-10-29
### Added

	- Support for translations.

### Changed

	- License changed to EUPL v1.2.
	- mod.conf set to follow MT v5.x specifics.

### Removed

	- Support for MT v0.4.x
