--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

-- Global mod's namespace
ores_stats = {}

-- Database handler
ores_stats.t_MOD_DATABASE = minetest.get_mod_storage()

-- Default ore value
ores_stats.f_GOLD_VALUE = minetest.settings:get('ores_stats_gold_value')
	or 50.0

-- Support for More Ores
ores_stats.b_MORE_ORES = false

if (minetest.get_modpath('moreores') ~= nil) then
	ores_stats.b_MORE_ORES = true
end


--
-- Variables
--

ores_stats.i_TotalOres = 0



--
-- Procedures
--

-- Minetest logger
local pr_LogMessage = function()

	-- Constant
	local s_LOG_LEVEL = minetest.settings:get('debug_log_level')

	-- Body
	if (s_LOG_LEVEL == nil)
	or (s_LOG_LEVEL == 'action')
	or (s_LOG_LEVEL == 'info')
	or (s_LOG_LEVEL == 'verbose')
	then
		minetest.log('action', '[Mod] Ores Stats [v1.1.0] loaded.')
	end
end


-- Subfiles loader
local pr_LoadSubFiles = function()

	-- Constant
	local s_MOD_PATH = minetest.get_modpath('ores_stats')

	-- Body
	dofile(s_MOD_PATH .. '/core/functions.lua')
	dofile(s_MOD_PATH .. '/core/procedures.lua')
	dofile(s_MOD_PATH .. '/core/minetest_game.lua')

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		dofile(s_MOD_PATH .. '/core/moreores_procedures.lua')
		dofile(s_MOD_PATH .. '/core/moreores.lua')
	end

	dofile(s_MOD_PATH .. '/core/formspec.lua')

end


--
-- Main body
--

pr_LoadSubFiles()

ores_stats.pr_MTGupdateFromDB()
ores_stats.pr_MTGupdateFromDBperc()
ores_stats.pr_MTGupdateFromDBvalue()

-- Support for More Ores' nodes
if (ores_stats.b_MORE_ORES == true) then
	ores_stats.pr_MOupdateFromDB()
	ores_stats.pr_MOupdateFromDBperc()
	ores_stats.pr_MOupdateFromDBvalue()
end

-- Voxel manipulator
minetest.register_on_generated(function()

	local vm, emin, emax = minetest.get_mapgen_object"voxelmanip"
	local data = vm:get_data()
	local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}

	for i in area:iterp(emin, emax) do
		ores_stats.pr_OresScanner(data[i])
	end

	ores_stats.pr_CalcTotalOres()
	ores_stats.pr_OresPercentages()
	ores_stats.pr_CalcValues()

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		ores_stats.pr_MOCalcValues()
	end


	ores_stats.pr_MTGupdateSaveDB()
	ores_stats.pr_MTGupdateSaveDBperc()
	ores_stats.pr_MTGupdateSaveDBvalue()

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		ores_stats.pr_MOupdateSaveDB()
		ores_stats.pr_MOupdateSaveDBperc()
		ores_stats.pr_MOupdateSaveDBvalue()
	end


	ores_stats.pr_UpdateFormspec()

end)

pr_LogMessage()
