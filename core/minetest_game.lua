--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

ores_stats.id_coal = minetest.get_content_id('default:stone_with_coal')
ores_stats.id_copper = minetest.get_content_id('default:stone_with_copper')
ores_stats.id_diamonds = minetest.get_content_id('default:stone_with_diamond')
ores_stats.id_gold = minetest.get_content_id('default:stone_with_gold')
ores_stats.id_iron = minetest.get_content_id('default:stone_with_iron')
ores_stats.id_mese = minetest.get_content_id('default:stone_with_mese')
ores_stats.id_mese_block = minetest.get_content_id('default:mese')
ores_stats.id_tin = minetest.get_content_id('default:stone_with_tin')


--
-- Variables
--

ores_stats.i_foundCoal = 0
ores_stats.i_foundCopper = 0
ores_stats.i_foundDiamonds = 0
ores_stats.i_foundGold = 0
ores_stats.i_foundIron = 0
ores_stats.i_foundMese = 0
ores_stats.i_foundTin = 0

ores_stats.f_percCoal = 0.0
ores_stats.f_percCopper = 0.0
ores_stats.f_percDiamonds = 0.0
ores_stats.f_percGold = 0.0
ores_stats.f_percIron = 0.0
ores_stats.f_percMese = 0.0
ores_stats.f_percTin = 0.0

ores_stats.f_valueCoal = 0.0
ores_stats.f_valueCopper = 0.0
ores_stats.f_valueDiamonds = 0.0
ores_stats.f_valueGold = 0.0
ores_stats.f_valueIron = 0.0
ores_stats.f_valueMese = 0.0
ores_stats.f_valueTin = 0.0
