--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Used to update the Minetest Game's variables
ores_stats.pr_MTGupdateFromDB = function()

	ores_stats.i_foundCoal =
		ores_stats.t_MOD_DATABASE:get_int('i_found_coal') or 0

	ores_stats.i_foundCopper =
		ores_stats.t_MOD_DATABASE:get_int('i_found_copper') or 0

	ores_stats.i_foundDiamonds =
		ores_stats.t_MOD_DATABASE:get_int('i_found_diamonds') or 0

	ores_stats.i_foundGold =
		ores_stats.t_MOD_DATABASE:get_int('i_found_gold') or 0

	ores_stats.i_foundIron =
		ores_stats.t_MOD_DATABASE:get_int('i_found_iron') or 0

	ores_stats.i_foundMese =
		ores_stats.t_MOD_DATABASE:get_int('i_found_mese') or 0

	ores_stats.i_foundTin =
		ores_stats.t_MOD_DATABASE:get_int('i_found_tin') or 0
end

-- Used to save the Minetest Game's variables
ores_stats.pr_MTGupdateSaveDB = function()

	ores_stats.t_MOD_DATABASE:set_int('i_found_coal', ores_stats.i_foundCoal)

	ores_stats.t_MOD_DATABASE:set_int('i_found_copper',
		ores_stats.i_foundCopper)

	ores_stats.t_MOD_DATABASE:set_int('i_found_diamonds',
		ores_stats.i_foundDiamonds)

	ores_stats.t_MOD_DATABASE:set_int('i_found_gold', ores_stats.i_foundGold)

	ores_stats.t_MOD_DATABASE:set_int('i_found_iron', ores_stats.i_foundIron)

	ores_stats.t_MOD_DATABASE:set_int('i_found_mese', ores_stats.i_foundMese)

	ores_stats.t_MOD_DATABASE:set_int('i_found_tin', ores_stats.i_foundTin)
end

-- Used to update the Minetest Game's percentages variables
ores_stats.pr_MTGupdateFromDBperc = function()

	ores_stats.f_percCoal =
		ores_stats.t_MOD_DATABASE:get_float('f_percCoal') or 0.0

	ores_stats.f_percCopper =
		ores_stats.t_MOD_DATABASE:get_float('f_percCopper') or 0.0

	ores_stats.f_percDiamonds =
		ores_stats.t_MOD_DATABASE:get_float('f_percDiamonds') or 0.0

	ores_stats.f_percGold =
		ores_stats.t_MOD_DATABASE:get_float('f_percGold') or 0.0

	ores_stats.f_percIron =
		ores_stats.t_MOD_DATABASE:get_float('f_percIron') or 0.0

	ores_stats.f_percMese =
		ores_stats.t_MOD_DATABASE:get_float('f_percMese') or 0.0

	ores_stats.f_percTin =
		ores_stats.t_MOD_DATABASE:get_float('f_percTin') or 0.0

end

-- Used to save the Minetest Game's percentages variables
ores_stats.pr_MTGupdateSaveDBperc = function()

		ores_stats.t_MOD_DATABASE:set_float('f_percCoal', ores_stats.f_percCoal)

		ores_stats.t_MOD_DATABASE:set_float('f_percCopper',
			ores_stats.f_percCopper)

		ores_stats.t_MOD_DATABASE:set_float('f_percDiamonds',
			ores_stats.f_percDiamonds)

		ores_stats.t_MOD_DATABASE:set_float('f_percGold', ores_stats.f_percGold)

		ores_stats.t_MOD_DATABASE:set_float('f_percIron', ores_stats.f_percIron)

		ores_stats.t_MOD_DATABASE:set_float('f_percMese', ores_stats.f_percMese)

		ores_stats.t_MOD_DATABASE:set_float('f_percTin', ores_stats.f_percTin)
end

-- Used to update the total ores found variable
ores_stats.pr_CalcTotalOres = function()

	ores_stats.i_TotalOres = (
		ores_stats.i_foundCoal + ores_stats.i_foundCopper +
		ores_stats.i_foundDiamonds + ores_stats.i_foundGold +
		ores_stats.i_foundIron + ores_stats.i_foundMese +
		ores_stats.i_foundTin
	)

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		ores_stats.i_TotalOres = (
			ores_stats.i_TotalOres +
			ores_stats.i_foundMithril +
			ores_stats.i_foundSilver
		)
	end
end

-- Used to update the amounts of the ores found
ores_stats.pr_OresScanner = function(a_s_node_id)

	if (a_s_node_id == ores_stats.id_coal) then
		ores_stats.i_foundCoal = (ores_stats.i_foundCoal + 1)

	elseif (a_s_node_id == ores_stats.id_copper) then
		ores_stats.i_foundCopper = (ores_stats.i_foundCopper + 1)

	elseif (a_s_node_id == ores_stats.id_diamonds) then
		ores_stats.i_foundDiamonds = (ores_stats.i_foundDiamonds + 1)

	elseif (a_s_node_id == ores_stats.id_gold) then
		ores_stats.i_foundGold = (ores_stats.i_foundGold + 1)

	elseif (a_s_node_id == ores_stats.id_iron) then
		ores_stats.i_foundIron = (ores_stats.i_foundIron + 1)

	elseif (a_s_node_id == ores_stats.id_mese) then
		ores_stats.i_foundMese = (ores_stats.i_foundMese + 1)

	elseif (a_s_node_id == ores_stats.id_mese_block) then
		ores_stats.i_foundMese = (ores_stats.i_foundMese + 9)

	elseif (a_s_node_id == ores_stats.id_tin) then
		ores_stats.i_foundTin = (ores_stats.i_foundTin + 1)

	end

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		if (a_s_node_id == ores_stats.id_mithril) then
			ores_stats.i_foundMithril = (ores_stats.i_foundMithril + 1)

		elseif (a_s_node_id == ores_stats.id_silver) then
			ores_stats.i_foundSilver = (ores_stats.i_foundSilver + 1)

		end
	end
end

-- Used to update the amounts of the ores found
ores_stats.pr_OresPercentages = function()

	ores_stats.f_percCoal =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundCoal,
			ores_stats.i_TotalOres)

	ores_stats.f_percCopper =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundCopper,
			ores_stats.i_TotalOres)

	ores_stats.f_percDiamonds =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundDiamonds,
			ores_stats.i_TotalOres)

	ores_stats.f_percGold =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundGold,
			ores_stats.i_TotalOres)

	ores_stats.f_percIron =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundIron,
			ores_stats.i_TotalOres)

	ores_stats.f_percMese =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundMese,
			ores_stats.i_TotalOres)

	ores_stats.f_percMese =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundMese,
			ores_stats.i_TotalOres)

	ores_stats.f_percTin =
		ores_stats.fn_CalcPercentage(ores_stats.i_foundTin,
			ores_stats.i_TotalOres)

	-- Support for More Ores' nodes
	if (ores_stats.b_MORE_ORES == true) then
		ores_stats.f_percMithril =
			ores_stats.fn_CalcPercentage(ores_stats.i_foundMithril,
				ores_stats.i_TotalOres)

		ores_stats.f_percSilver =
			ores_stats.fn_CalcPercentage(ores_stats.i_foundSilver,
				ores_stats.i_TotalOres)
	end
end

-- Used to update the Minetest Game's value variables
ores_stats.pr_MTGupdateFromDBvalue = function()

	ores_stats.f_valueCoal =
		ores_stats.t_MOD_DATABASE:get_float('f_valueCoal') or 0.0

	ores_stats.f_valueCopper =
		ores_stats.t_MOD_DATABASE:get_float('f_valueCopper') or 0.0

	ores_stats.f_valueDiamonds =
		ores_stats.t_MOD_DATABASE:get_float('f_valueDiamonds') or 0.0

	ores_stats.f_valueGold =
		ores_stats.t_MOD_DATABASE:get_float('f_valueGold') or 0.0

	ores_stats.f_valueIron =
		ores_stats.t_MOD_DATABASE:get_float('f_valueIron') or 0.0

	ores_stats.f_valueMese =
		ores_stats.t_MOD_DATABASE:get_float('f_valueMese') or 0.0

	ores_stats.f_valueTin =
		ores_stats.t_MOD_DATABASE:get_float('f_valueTin') or 0.0

end

-- Used to save the Minetest Game's value variables
ores_stats.pr_MTGupdateSaveDBvalue = function()

		ores_stats.t_MOD_DATABASE:set_float('f_valueCoal',
			ores_stats.f_valueCoal)

		ores_stats.t_MOD_DATABASE:set_float('f_valueCopper',
			ores_stats.f_valueCopper)

		ores_stats.t_MOD_DATABASE:set_float('f_valueDiamonds',
			ores_stats.f_valueDiamonds)

		ores_stats.t_MOD_DATABASE:set_float('f_valueGold',
			ores_stats.f_valueGold)

		ores_stats.t_MOD_DATABASE:set_float('f_valueIron',
			ores_stats.f_valueIron)

		ores_stats.t_MOD_DATABASE:set_float('f_valueMese',
			ores_stats.f_valueMese)

		ores_stats.t_MOD_DATABASE:set_float('f_valueTin',
			ores_stats.f_valueTin)
end

-- Used to calculate the Minetest Game's ores values
ores_stats.pr_CalcValues = function()

	ores_stats.f_valueCoal = ores_stats.fn_CalcValue(ores_stats.f_percCoal)

	ores_stats.f_valueCopper = ores_stats.fn_CalcValue(ores_stats.f_percCopper)

	ores_stats.f_valueDiamonds =
		ores_stats.fn_CalcValue(ores_stats.f_percDiamonds)

	ores_stats.f_valueGold = ores_stats.fn_CalcValue(ores_stats.f_percGold)

	ores_stats.f_valueIron = ores_stats.fn_CalcValue(ores_stats.f_percIron)

	ores_stats.f_valueMese = ores_stats.fn_CalcValue(ores_stats.f_percMese)

	ores_stats.f_valueTin = ores_stats.fn_CalcValue(ores_stats.f_percTin)
end
