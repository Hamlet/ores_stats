--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Functions
--

-- Used to calculate an ore's percentage
ores_stats.fn_CalcPercentage = function(a_i_ore, a_i_total_ores)
	local f_percentage = 0.0

	if (a_i_total_ores ~= 0) then
		f_percentage = ((a_i_ore * 100) / a_i_total_ores)
	end

	return f_percentage
end

-- Used to calculate an ore's value
ores_stats.fn_CalcValue = function(a_f_ore_percentage)

	-- Constants
	local f_GOLD_PERC = 4.1		-- Average percentage

	-- Variables
	local f_goldPerc, f_goldValue, f_oreRatio, f_oreValue

	f_goldPerc = ores_stats.f_percGold

	if (f_goldPerc == 0) then
		f_goldPerc = f_GOLD_PERC
	end

	f_goldValue = ores_stats.f_valueGold

	if (f_goldValue == 0) then
		f_goldValue = ores_stats.f_GOLD_VALUE
	end

	if (a_f_ore_percentage ~= 0) then
		f_oreRatio = (a_f_ore_percentage / f_goldPerc)
		f_oreValue = (f_goldValue / f_oreRatio)

	else
		f_oreValue = 0.0

	end

	return f_oreValue
end
