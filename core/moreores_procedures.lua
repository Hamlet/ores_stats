--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Used to update the More Ores' variables
ores_stats.pr_MOupdateFromDB = function()

	ores_stats.i_foundMithril =
		ores_stats.t_MOD_DATABASE:get_int('i_found_mithril') or 0

	ores_stats.i_foundSilver =
		ores_stats.t_MOD_DATABASE:get_int('i_found_silver') or 0
end

-- Used to save the More Ores' variables
ores_stats.pr_MOupdateSaveDB = function()

	ores_stats.t_MOD_DATABASE:set_int('i_found_mithril',
		ores_stats.i_foundMithril)

	ores_stats.t_MOD_DATABASE:set_int('i_found_silver',
		ores_stats.i_foundSilver)
end

-- Used to update the More Ores' percentages variables
ores_stats.pr_MOupdateFromDBperc = function()

	ores_stats.f_percMithril =
		ores_stats.t_MOD_DATABASE:get_float('f_percMithril') or 0.0

	ores_stats.f_percSilver =
		ores_stats.t_MOD_DATABASE:get_float('f_percSilver') or 0.0

end

-- Used to save the More Ores' percentages variables
ores_stats.pr_MOupdateSaveDBperc = function()

		ores_stats.t_MOD_DATABASE:set_float('f_percMithril', 	
			ores_stats.f_percMithril)

		ores_stats.t_MOD_DATABASE:set_float('f_percSilver',
			ores_stats.f_percSilver)
end

-- Used to update the More Ores' value variables
ores_stats.pr_MOupdateFromDBvalue = function()

	ores_stats.f_valueMithril =
		ores_stats.t_MOD_DATABASE:get_float('f_valueMithril') or 0.0

	ores_stats.f_valueSilver =
		ores_stats.t_MOD_DATABASE:get_float('f_valueSilver') or 0.0
end

-- Used to save the More Ores' value variables
ores_stats.pr_MOupdateSaveDBvalue = function()

		ores_stats.t_MOD_DATABASE:set_float('f_valueMithril',
			ores_stats.f_valueMithril)

		ores_stats.t_MOD_DATABASE:set_float('f_valueSilver',
			ores_stats.f_valueSilver)
end

-- Used to calculate the More Ores' ores values
ores_stats.pr_MOCalcValues = function()

	ores_stats.f_valueMithril =
		ores_stats.fn_CalcValue(ores_stats.f_percMithril)

	ores_stats.f_valueSilver = ores_stats.fn_CalcValue(ores_stats.f_percSilver)
end
