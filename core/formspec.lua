--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization
local S = minetest.get_translator('ores_stats')


--
-- Formspec registration
--

sfinv.register_page('ores_stats:ores_stats', {
	title = 'Ores\' Stats',
	get = function(self, player, context)
		local s_oresPerc =
			',' .. '   ' ..
			S("Ores' percentages") .. ',' .. '   ' ..
			'========================' .. ',' .. '   ' ..
			S('Coal: ') .. string.format("%.3f", ores_stats.f_percCoal)
			.. '%' .. ',' .. '   ' ..
			S('Copper: ') .. string.format("%.3f", ores_stats.f_percCopper)
			.. '%' .. ',' .. '   ' ..
			S('Diamonds: ') .. string.format("%.3f", ores_stats.f_percDiamonds)
			.. '%' .. ',' .. '   ' ..
			S('Gold: ') .. string.format("%.3f", ores_stats.f_percGold)
			.. '%' .. ',' .. '   ' ..
			S('Iron: ') .. string.format("%.3f", ores_stats.f_percIron)
			.. '%' .. ',' .. '   ' ..
			S('Mese: ') .. string.format("%.3f", ores_stats.f_percMese)
			.. '%' .. ',' .. '   ' ..
			S('Tin: ') .. string.format("%.3f", ores_stats.f_percTin)
			.. '%' .. ','

		local s_oresValue =
			',' .. '   ' ..
			S("Ores' values") .. ',' .. '   ' ..
			'========================' .. ',' .. '   ' ..
			S('Coal: ') .. string.format("%.3f", ores_stats.f_valueCoal)
			.. ',' .. '   ' ..
			S('Copper: ') .. string.format("%.3f", ores_stats.f_valueCopper)
			.. ',' .. '   ' ..
			S('Diamonds: ') .. string.format("%.3f", ores_stats.f_valueDiamonds)
			.. ',' .. '   ' ..
			S('Gold: ') .. string.format("%.3f", ores_stats.f_valueGold)
			.. ',' .. '   ' ..
			S('Iron: ') .. string.format("%.3f", ores_stats.f_valueIron)
			.. ',' .. '   ' ..
			S('Mese: ') .. string.format("%.3f", ores_stats.f_valueMese)
			.. ',' .. '   ' ..
			S('Tin: ') .. string.format("%.3f", ores_stats.f_valueTin)
			.. ','

		-- Support for More Ores' nodes
		if (ores_stats.b_MORE_ORES == true) then
			s_oresPerc = s_oresPerc .. '   ' ..
				S('Mithril: ') .. string.format("%.3f",
					ores_stats.f_percMithril)
				.. '%' .. ',' .. '   ' ..
				S('Silver: ') .. string.format("%.3f", ores_stats.f_percSilver)
				.. '%' .. ','

			s_oresValue = s_oresValue  .. '   ' ..
				S('Mithril: ') .. string.format("%.3f",
					ores_stats.f_valueMithril)
				.. ',' .. '   ' ..
				S('Silver: ') .. string.format("%.3f", ores_stats.f_valueSilver)
				.. ','
		end


		return sfinv.make_formspec(player, context,
				'textlist[0,0;7.8,9.2;;' ..
				s_oresPerc .. s_oresValue .. ']', false)
	end
})


--
-- Formspec updating
--

ores_stats.pr_UpdateFormspec = function()
	sfinv.override_page('ores_stats:ores_stats', {
		title = 'Ores\' Stats',
		get = function(self, player, context)
			local s_oresPerc =
				',' .. '   ' ..
				S("Ores' percentages") .. ',' .. '   ' ..
				'========================' .. ',' .. '   ' ..
				S('Coal: ') .. string.format("%.3f", ores_stats.f_percCoal)
				.. '%' .. ',' .. '   ' ..
				S('Copper: ') .. string.format("%.3f", ores_stats.f_percCopper)
				.. '%' .. ',' .. '   ' ..
				S('Diamonds: ') .. string.format("%.3f",
					ores_stats.f_percDiamonds)
				.. '%' .. ',' .. '   ' ..
				S('Gold: ') .. string.format("%.3f", ores_stats.f_percGold)
				.. '%' .. ',' .. '   ' ..
				S('Iron: ') .. string.format("%.3f", ores_stats.f_percIron)
				.. '%' .. ',' .. '   ' ..
				S('Mese: ') .. string.format("%.3f", ores_stats.f_percMese)
				.. '%' .. ',' .. '   ' ..
				S('Tin: ') .. string.format("%.3f", ores_stats.f_percTin)
				.. '%' .. ','

			local s_oresValue =
				',' .. '   ' ..
				S("Ores' values") .. ',' .. '   ' ..
				'========================' .. ',' .. '   ' ..
				S('Coal: ') .. string.format("%.3f", ores_stats.f_valueCoal)
				.. ',' .. '   ' ..
				S('Copper: ') .. string.format("%.3f", ores_stats.f_valueCopper)
				.. ',' .. '   ' ..
				S('Diamonds: ') .. string.format("%.3f",
					ores_stats.f_valueDiamonds)
				.. ',' .. '   ' ..
				S('Gold: ') .. string.format("%.3f", ores_stats.f_valueGold)
				.. ',' .. '   ' ..
				S('Iron: ') .. string.format("%.3f", ores_stats.f_valueIron)
				.. ',' .. '   ' ..
				S('Mese: ') .. string.format("%.3f", ores_stats.f_valueMese)
				.. ',' .. '   ' ..
				S('Tin: ') .. string.format("%.3f", ores_stats.f_valueTin)
				.. ','

				-- Support for More Ores' nodes
				if (ores_stats.b_MORE_ORES == true) then
					s_oresPerc = s_oresPerc .. '   ' ..
						S('Mithril: ') .. string.format("%.3f",
							ores_stats.f_percMithril)
						.. '%' .. ',' .. '   ' ..
						S('Silver: ') .. string.format("%.3f",
							ores_stats.f_percSilver)
						.. '%' .. ','

					s_oresValue = s_oresValue  .. '   ' ..
						S('Mithril: ') .. string.format("%.3f",
							ores_stats.f_valueMithril)
						.. ',' .. '   ' ..
						S('Silver: ') .. string.format("%.3f", 
							ores_stats.f_valueSilver)
						.. ','
				end


			return sfinv.make_formspec(player, context,
					'textlist[0,0;7.8,9.2;;' ..
					s_oresPerc .. s_oresValue .. ']', false)
		end
	})
end
