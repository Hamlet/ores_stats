--[[
    Ores' Statistics - Provides informations about ores' percentages
	Copyright © 2018, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

ores_stats.id_mithril = minetest.get_content_id('moreores:mineral_mithril')
ores_stats.id_silver = minetest.get_content_id('moreores:mineral_silver')


--
-- Variables
--

ores_stats.i_foundMithril = 0
ores_stats.i_foundSilver = 0

ores_stats.f_percMithril = 0.0
ores_stats.f_percSilver = 0.0

ores_stats.f_valueMithril = 0.0
ores_stats.f_valueSilver = 0.0
